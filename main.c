#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

#define SHELL "/usr/bin/zsh"
#define STORAGE "/home/drahflow/.tape"
#define BUILD_STORAGE STORAGE "/build"
#define INSTALL_STORAGE STORAGE "/install"
#define RUN_STORAGE STORAGE "/run"
#define WORK_STORAGE STORAGE "/work"
#define TARGET_STORAGE STORAGE "/target"

void append(char *buffer, int buflen, char **dst, char *src) {
  int len = strlen(src);
  if(*dst + len >= buffer + buflen) {
    fprintf(stderr, "Too many arguments. Buffer is full.\n");
    assert(0);
  }

  strcpy(*dst, src);
  *dst += len;
}

int main(int argc, char *const *argv) {
  char *layers[256];
  unsigned int iLayer = 0;

  char *pkgName = NULL;
  int build = 0;
  int install = 0;
  int run = 0;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"layer",   required_argument, 0,  0 },
      {"build",   no_argument,       0,  0 },
      {"install", no_argument,       0,  0 },
      {"run",     no_argument,       0,  0 },
      {0,         0,                 0,  0 }
    };

    int c = getopt_long(argc, argv, "l:bir", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
      case 'l': option_index = 0; goto process_long;
      case 'b': option_index = 1; goto process_long;
      case 'i': option_index = 2; goto process_long;
      case 'r': option_index = 3; goto process_long;

      case 0: process_long:
        printf("option %s", long_options[option_index].name);
        if (optarg)
          printf(" with arg %s", optarg);
        printf("\n");

        switch(option_index) {
          case 0:
            if(iLayer >= sizeof(layers) / sizeof(*layers)) {
              fprintf(stderr, "Too many layers\n");
              return 1;
            }

            layers[iLayer++] = optarg;
            break;
          
          case 1: build = 1; break;
          case 2: install = 1; break;
          case 3: run = 1; break;

          default:
                  assert(0);
        }
        break;

      default:
        printf("?? getopt returned character code 0%o ??\n", c);
    }
  }

  if (optind < argc) {
    pkgName = argv[optind++];
  }

  if (optind < argc) {
    printf("non-option ARGV-elements: ");
    for(int i = optind; i < argc; ++i) {
      printf("%s ", argv[i]);
    }
    printf("\n");
  }

  char buffer[65536];
  char *dst = buffer;

  // FIXME: Buffer size
  for(unsigned int i = iLayer - 1; i < iLayer; --i) {
    if(dst == buffer) {
      append(buffer, sizeof(buffer), &dst, "lowerdir=");
    } else {
      *dst++ = ':';
    }

    if(!strcmp(layers[i], "/")) {
      append(buffer, sizeof(buffer), &dst, "/");
    } else {
      append(buffer, sizeof(buffer), &dst, RUN_STORAGE);
      append(buffer, sizeof(buffer), &dst, "/");
      append(buffer, sizeof(buffer), &dst, layers[i]);
      append(buffer, sizeof(buffer), &dst, ":");
      append(buffer, sizeof(buffer), &dst, INSTALL_STORAGE);
      append(buffer, sizeof(buffer), &dst, "/");
      append(buffer, sizeof(buffer), &dst, layers[i]);
    }
  }

  if(build) {
    if(dst != buffer) *dst++ = ',';
    append(buffer, sizeof(buffer), &dst, "upperdir=");
    append(buffer, sizeof(buffer), &dst, BUILD_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
    append(buffer, sizeof(buffer), &dst, ",");
    append(buffer, sizeof(buffer), &dst, "workdir=");
    append(buffer, sizeof(buffer), &dst, WORK_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
  }

  if(install) {
    if(dst != buffer) *dst++ = ':';
    append(buffer, sizeof(buffer), &dst, BUILD_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
    append(buffer, sizeof(buffer), &dst, ",upperdir=");
    append(buffer, sizeof(buffer), &dst, INSTALL_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
    append(buffer, sizeof(buffer), &dst, ",workdir=");
    append(buffer, sizeof(buffer), &dst, WORK_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
  }

  if(run) {
    if(dst != buffer) *dst++ = ':';
    append(buffer, sizeof(buffer), &dst, INSTALL_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
    append(buffer, sizeof(buffer), &dst, ",upperdir=");
    append(buffer, sizeof(buffer), &dst, RUN_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
    append(buffer, sizeof(buffer), &dst, ",workdir=");
    append(buffer, sizeof(buffer), &dst, WORK_STORAGE);
    append(buffer, sizeof(buffer), &dst, "/");
    append(buffer, sizeof(buffer), &dst, pkgName);
  }

  char target[4096];
  snprintf(target, sizeof(target), "%s/%s", BUILD_STORAGE, pkgName);
  if(mkdir(target, 0777) == -1 && errno != EEXIST) {
    fprintf(stderr, "Cannot create %s: %s", target, strerror(errno));
    return 1;
  }
  snprintf(target, sizeof(target), "%s/%s", INSTALL_STORAGE, pkgName);
  if(mkdir(target, 0777) == -1 && errno != EEXIST) {
    fprintf(stderr, "Cannot create %s: %s", target, strerror(errno));
    return 1;
  }
  snprintf(target, sizeof(target), "%s/%s", RUN_STORAGE, pkgName);
  if(mkdir(target, 0777) == -1 && errno != EEXIST) {
    fprintf(stderr, "Cannot create %s: %s", target, strerror(errno));
    return 1;
  }
  snprintf(target, sizeof(target), "%s/%s", WORK_STORAGE, pkgName);
  if(mkdir(target, 0777) == -1 && errno != EEXIST) {
    fprintf(stderr, "Cannot create %s: %s", target, strerror(errno));
    return 1;
  }
  snprintf(target, sizeof(target), "%s/%d", TARGET_STORAGE, getpid());
  if(mkdir(target, 0777) == -1 && errno != EEXIST) {
    fprintf(stderr, "Cannot create %s: %s", target, strerror(errno));
    return 1;
  }

  if(mount("overlay", target, "overlay", 0, buffer) == -1) {
    fprintf(stderr, "Failed overlay mount: %s", strerror(errno));
    return 1;
  }

  int childPid = fork();
  if(childPid == -1) {
    fprintf(stderr, "Failed to fork: %s", strerror(errno));
    return 1;
  }

  if(!childPid) {
    char cwd[4096];
    if(!getcwd(cwd, sizeof(cwd))) {
      fprintf(stderr, "Failed to getcwd: %s", strerror(errno));
      return 1;
    }

    if(chroot(target) == -1) {
      fprintf(stderr, "Failed to chroot: %s", strerror(errno));
      return 1;
    }

    const char *pwd;
    if((pwd = getenv("PWD"))) {
      if(chdir(pwd) == -1) {
        if(chdir(cwd) == -1) {
          fprintf(stderr, "Failed to chdir to %s (after %s): %s", cwd, pwd, strerror(errno));
          return 1;
        }
      }
    }

    if(optind == argc) {
      char *const noArgs[] = {SHELL, NULL};
      execv(SHELL, noArgs);
    } else {
      execvp(argv[optind], argv + optind);
    }

    assert(0);
  }

  if(waitpid(childPid, NULL, 0) == -1) {
    fprintf(stderr, "Failed to waitpid: %s", strerror(errno));
    return 1;
  }

  if(umount(target) == -1) {
    fprintf(stderr, "Failed to unmount %s: %s", target, strerror(errno));
    return 1;
  }

  if(rmdir(target) == -1) {
    fprintf(stderr, "Failed to rmdir %s: %s", target, strerror(errno));
    return 1;
  }
}
